#ifndef Pipeline_h__
#define Pipeline_h__

#include "Engine_Include.h"

BEGIN(Engine)

class ENGINE_DLL CPipeline
{
public:
	CPipeline(void);
	~CPipeline(void);

public:
	static void MakeWorldMatrix(D3DXMATRIX* pOut
		, const D3DXVECTOR3* pScale
		, const float* pAngle
		, const D3DXVECTOR3* pPos);

	static void MakeTransformMatrix(D3DXMATRIX* pOut
		, const D3DXVECTOR3* pRight
		, const D3DXVECTOR3* pUp
		, const D3DXVECTOR3* pLook
		, const D3DXVECTOR3* pPos);

	static void MakeProjectionMatrix(D3DXMATRIX* pOut
		, const float& fFovY
		, const float& fAspect
		, const float& fNear
		, const float& fFar);

	static void MakeViewMatrix(D3DXMATRIX* pOut
		, const D3DXVECTOR3* pPos
		, const D3DXVECTOR3* pTarget
		, const D3DXVECTOR3* pUp);
};

END

#endif // Pipeline_h__