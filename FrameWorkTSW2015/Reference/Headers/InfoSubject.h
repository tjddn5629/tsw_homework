#ifndef InfoSubject_h__
#define InfoSubject_h__

#include "Subject.h"

BEGIN(Engine)

class ENGINE_DLL CInfoSubject :
	public CSubject
{
	DECLARE_SINGLETON(CInfoSubject);

private:
	map<int, list<void*>*>		m_mapData;

public:
	void AddData(int iMessage, void* pData);
	void RemoveData(int iMessage, void* pData);
	void Release(void);

public:
	list<void*>* GetDataList(int iMessage)
	{
		map<int, list<void*>*>::iterator iter = m_mapData.find(iMessage);

		if (iter == m_mapData.end())
			return NULL;

		return iter->second;
	}

private:
	CInfoSubject(void);
	virtual ~CInfoSubject(void);
};

END

#endif