#ifndef SceneMgr_h__
#define SceneMgr_h__

#include "Engine_Include.h"

BEGIN(Engine)

class CScene;
class ENGINE_DLL CSceneMgr
{
public:
	DECLARE_SINGLETON(CSceneMgr)

public:
	void Initialize(void);
	void Update(void);
	void Render();
	void Release(void);

public:
	/*template<typename T> void SetScene(eSCENE_TYPE eType)
	{
		if (m_pScene)
		{
			delete m_pScene;
			m_pScene = NULL;
		}

		m_eType = eType;

		m_pScene = new T;
		m_pScene->Initialize();
	}*/

	void SetScene(eSCENE_TYPE eType, CScene* pScene);


	void SetChangeScene(eSCENE_TYPE eType, int iIndex = 0);
	eSCENE_TYPE GetSceneType(void) { return m_eType; }
	eSCENE_TYPE GetSceneBeforeType(void) { return m_eTypeBefore; }

public:
	CSceneMgr();
	~CSceneMgr();

private:
	CScene*		m_pScene;
	bool		m_bChangeScene;
	eSCENE_TYPE	m_eType;
	eSCENE_TYPE m_eTypeBefore;
};



END

#endif // !SceneMgr_h__

