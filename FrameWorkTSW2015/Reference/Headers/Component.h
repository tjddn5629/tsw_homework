#ifndef Component_h__
#define Component_h__

#include "Engine_Include.h"

BEGIN(Engine)

class ENGINE_DLL CComponent abstract
{
protected:
	CComponent(void);
public:
	virtual ~CComponent(void);

public:
	virtual void Update(void) {}
};

END

#endif // !Component_h__