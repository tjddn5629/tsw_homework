CKeyMgr* Get_KeyMgr(void)
{
	return CKeyMgr::GetInstance();
}

CManagement* Get_Management(void)
{
	return CManagement::GetInstance();
}

CInfoSubject* Get_SubjectMgr(void)
{
	return CInfoSubject::GetInstance();
}