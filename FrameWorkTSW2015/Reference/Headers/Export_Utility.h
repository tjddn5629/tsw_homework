#ifndef Export_Utility_h__
#define Export_Utility_h__

#include "KeyMgr.h"
#include "Management.h"
#include "InfoSubject.h" 

BEGIN(Engine)

inline CKeyMgr* Get_KeyMgr(void);
inline CManagement* Get_Management(void);
inline CInfoSubject* Get_SubjectMgr(void);

#include "Export_Utility.inl"

END

#endif // !Export_Utility_h__
