#ifndef CLandColor_h__
#define CLandColor_h__

#include "VIBuffer.h"

BEGIN(Engine)

class ENGINE_DLL CLandColor :
	public CVIBuffer
{
private:
	explicit CLandColor(LPDIRECT3DDEVICE9 pDevice);

public:
	virtual ~CLandColor(void);

public:
	virtual CResources* CloneResource(void);

public:
	virtual HRESULT CreateBuffer(void);
	HRESULT CreateBuffer(DWORD dwVCX, DWORD dwVCZ);

private:
	void SetBuffers(void);
	void SetVertex(Engine::VTXCOL* pVertex, int iIndex, float fx, float fy, float fz, DWORD dwRed, DWORD dwGreen, DWORD dwBlue);
	void SetIndex(Engine::INDEX16* pIndex, int iIndex, int i_1, int i_2, int i_3);

public:
	static CLandColor* Create(LPDIRECT3DDEVICE9 pDevice, DWORD dwVCX, DWORD dwVCZ);

private:
	DWORD		m_dwVCX;
	DWORD		m_dwVCZ;
};

END

#endif // CLandColor_h__