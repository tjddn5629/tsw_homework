#ifndef CLandTex_h__
#define CLandTex_h__

#include "VIBuffer.h"

BEGIN(Engine)

class ENGINE_DLL CLandTex :
	public CVIBuffer
{
private:
	explicit CLandTex(LPDIRECT3DDEVICE9 pDevice);

public:
	virtual ~CLandTex(void);

public:
	virtual CResources* CloneResource(void);

public:
	virtual HRESULT CreateBuffer(void);
	HRESULT CreateBuffer(const WORD& wCntX, const WORD& wCntZ, const WORD& wItv);

private:
	void SetBuffers(void);
	void SetVertex(Engine::VTXTEX* pVertex, int iIndex, float fx, float fy, float fz);
	void SetIndex(Engine::INDEX32* pIndex, int iIndex, int i_1, int i_2, int i_3);

public:
	static CLandTex* Create(LPDIRECT3DDEVICE9 pDevice
		, const WORD& wCntX, const WORD& wCntZ, const WORD& wItv);

private:
	WORD		m_wVCX;
	WORD		m_wVCZ;
	WORD		m_wItv;
};

END

#endif