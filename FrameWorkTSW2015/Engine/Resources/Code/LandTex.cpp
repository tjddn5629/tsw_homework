#include "LandTex.h"


Engine::CLandTex::CLandTex(LPDIRECT3DDEVICE9 pDevice)
:CVIBuffer(pDevice)
{
}


Engine::CLandTex::~CLandTex()
{
	Release();
}

Engine::CResources * Engine::CLandTex::CloneResource(void)
{
	CResources*		pResource = new CLandTex(*this);

	++(*m_pwRefCnt);

	return pResource;
}

HRESULT Engine::CLandTex::CreateBuffer(void)
{
	return S_OK;
}

HRESULT Engine::CLandTex::CreateBuffer(const WORD& wCntX, const WORD& wCntZ, const WORD& wItv)
{
	m_wItv = wItv;
	m_wVCX = wCntX;
	m_wVCZ = wCntZ;

	m_dwVtxSize = sizeof(VTXTEX);
	m_dwVtxCnt = wCntX * wCntZ;
	m_dwVtxFVF = VTXTEX_FVF;

	m_dwIdxSize = sizeof(INDEX32);
	m_IdxFmt = D3DFMT_INDEX32;
	m_dwTriCnt = (wCntX - 1) * (wCntZ - 1) * 2;

	HRESULT		hr = CVIBuffer::CreateBuffer();
	FAILED_CHECK(hr);

	SetBuffers();

	return S_OK;
}

void Engine::CLandTex::SetBuffers(void)
{
	// 흑백이미지 픽셀 음영값에 따라 지형 버텍스의 y값 적용을 위해 
	// bmp이미지를 읽어온다.
	HANDLE	hFile;
	DWORD	dwByte;

	hFile = CreateFile(L"../bin/Texture/Terrain/Height.bmp", GENERIC_READ
		, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

	BITMAPFILEHEADER		fh;
	BITMAPINFOHEADER		ih;

	ReadFile(hFile, &fh, sizeof(BITMAPFILEHEADER), &dwByte, NULL);
	ReadFile(hFile, &ih, sizeof(BITMAPINFOHEADER), &dwByte, NULL);

	// 픽셀정보(음영정보)를 저장하기위한 포인터배열
	DWORD*		pdwPixel = new DWORD[ih.biWidth * ih.biHeight];

	ReadFile(hFile, pdwPixel, sizeof(DWORD) * ih.biWidth * ih.biHeight, &dwByte, NULL);
	CloseHandle(hFile);

	int		iIndex = 0;

	for (int i = 0; i < 2; ++i)
	{		
		// 버텍스 설정
		{
			VTXTEX*		pVtxTex = NULL;

			m_pVB[i]->Lock(0, 0, (void**)&pVtxTex, 0);
			
			for (int z = 0; z < m_wVCX; ++z)
			{
				for (int x = 0; x < m_wVCZ; ++x)
				{
					iIndex = z * m_wVCX + x;

					float	fy = (pdwPixel[iIndex] & 0x000000ff) / 100.f;
					SetVertex(pVtxTex, iIndex, x, fy, z);
				}
			}

			m_pVB[i]->Unlock();
		}
	}

	// 인덱스 설정
	{
		INDEX32*		pIndex = NULL;
		m_pIB->Lock(0, 0, (void**)&pIndex, 0);

		int		iTriCnt = 0;
		for (int z = 0; z < m_wVCZ - 1; ++z)
		{
			for (int x = 0; x < m_wVCX - 1; ++x)
			{
				iIndex = z * m_wVCX + x;
				SetIndex(pIndex, iTriCnt,
					iIndex + m_wVCX,
					iIndex + m_wVCX + 1,
					iIndex + 1);
				++iTriCnt;

				SetIndex(pIndex, iTriCnt,
					iIndex + m_wVCX,
					iIndex + 1,
					iIndex);
				++iTriCnt;
			}
		}

		m_pIB->Unlock();
	}
	
	Safe_Delete(pdwPixel);
}

void Engine::CLandTex::SetVertex(Engine::VTXTEX * pVertex, int iIndex, float fx, float fy, float fz)
{
	pVertex[iIndex].vPos = D3DXVECTOR3(fx * m_wItv, fy, fz * m_wItv);
	float fX = float(iIndex % m_wVCX) / float(m_wVCX - 1.f);
	float fZ = float(iIndex / m_wVCX) / float(m_wVCX - 1.f);
	pVertex[iIndex].vTexUV = D3DXVECTOR2(fX, fZ);
}

void Engine::CLandTex::SetIndex(Engine::INDEX32 * pIndex, int iIndex, int i_1, int i_2, int i_3)
{
	pIndex[iIndex]._1 = i_1;
	pIndex[iIndex]._2 = i_2;
	pIndex[iIndex]._3 = i_3;
}

Engine::CLandTex * Engine::CLandTex::Create(LPDIRECT3DDEVICE9 pDevice
	, const WORD& wCntX, const WORD& wCntZ, const WORD& wItv)
{
	CLandTex* pBuffer = new CLandTex(pDevice);
	if (FAILED(pBuffer->CreateBuffer(wCntX, wCntZ, wItv)))
		Safe_Delete(pBuffer);

	return pBuffer;
}
