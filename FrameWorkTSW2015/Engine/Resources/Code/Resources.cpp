#include "Resources.h"

Engine::CResources::CResources(LPDIRECT3DDEVICE9 pDevice)
	: m_pDevice(pDevice)
	, m_pwRefCnt(new WORD(0))
{
}

Engine::CResources::~CResources()
{
	Release();
}

void Engine::CResources::Release(void)
{
	if (0 == (*m_pwRefCnt))
		Safe_Delete(m_pwRefCnt);
	else
		--(*m_pwRefCnt);
}
