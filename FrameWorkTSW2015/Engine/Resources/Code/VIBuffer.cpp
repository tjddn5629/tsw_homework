#include "VIBuffer.h"

Engine::CVIBuffer::CVIBuffer(LPDIRECT3DDEVICE9 pDevice)
: CResources(pDevice)
, m_dwVtxSize(0)
, m_dwVtxCnt(0)
, m_dwVtxFVF(0)
, m_pIB(NULL)
, m_dwIdxSize(0)
, m_dwTriCnt(0)
, m_IdxFmt(D3DFMT_UNKNOWN)
, m_pMat(NULL)
{
	m_pVB[0] = NULL;
	m_pVB[1] = NULL;
}

Engine::CVIBuffer::~CVIBuffer()
{
	Release();
}

HRESULT Engine::CVIBuffer::CreateBuffer(void)
{
	HRESULT		hr = NULL;
	hr = m_pDevice->CreateVertexBuffer(m_dwVtxSize * m_dwVtxCnt, 0, m_dwVtxFVF, D3DPOOL_MANAGED, &m_pVB[0], NULL);
	FAILED_CHECK(hr);

	hr = m_pDevice->CreateVertexBuffer(m_dwVtxSize * m_dwVtxCnt, 0, m_dwVtxFVF, D3DPOOL_MANAGED, &m_pVB[1], NULL);
	FAILED_CHECK(hr);

	hr = m_pDevice->CreateIndexBuffer(m_dwIdxSize * m_dwTriCnt, 0, m_IdxFmt, D3DPOOL_MANAGED, &m_pIB, NULL);
	FAILED_CHECK(hr);
	
	return S_OK;
}

void Engine::CVIBuffer::SetRenderingPipeline(void)
{
	VTXTEX*		pVtxTex0 = NULL;
	VTXTEX*		pVtxTex1 = NULL;
	m_pVB[0]->Lock(0, 0, (void**)&pVtxTex0, 0);
	m_pVB[1]->Lock(0, 0, (void**)&pVtxTex1, 0);

	for (int i = 0; i < m_dwVtxCnt; ++i)
		D3DXVec3TransformCoord(&(pVtxTex0[i].vPos), &(pVtxTex1[i].vPos), m_pMat);

	m_pVB[0]->Unlock();
	m_pVB[1]->Unlock();
}

void Engine::CVIBuffer::Render(void)
{
	if (m_pMat)
		SetRenderingPipeline();

	m_pDevice->SetStreamSource(0, m_pVB[0], 0, m_dwVtxSize);
	m_pDevice->SetFVF(m_dwVtxFVF);
	m_pDevice->SetIndices(m_pIB);
	m_pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_dwVtxCnt, 0, m_dwTriCnt);
}

void Engine::CVIBuffer::Release(void)
{
	if (*m_pwRefCnt == 0)
	{
		Safe_Release(m_pVB[0]);
		Safe_Release(m_pVB[1]);
		Safe_Release(m_pIB);
	}
}
