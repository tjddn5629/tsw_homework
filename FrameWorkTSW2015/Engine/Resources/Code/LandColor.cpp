#include "LandColor.h"



Engine::CLandColor::CLandColor(LPDIRECT3DDEVICE9 pDevice)
:CVIBuffer(pDevice)
{
}

Engine::CLandColor::~CLandColor()
{
	Release();
}

Engine::CResources * Engine::CLandColor::CloneResource(void)
{
	CResources*		pResource = new CLandColor(*this);

	++(*m_pwRefCnt);

	return pResource;
}

HRESULT Engine::CLandColor::CreateBuffer(void)
{
	return S_OK;
}

HRESULT Engine::CLandColor::CreateBuffer(DWORD dwVCX, DWORD dwVCZ)
{
	m_dwVCX = dwVCX;
	m_dwVCZ = dwVCZ;

	m_dwVtxSize = sizeof(VTXCOL);
	m_dwVtxCnt = dwVCX * dwVCZ;
	m_dwVtxFVF = VTXCOL_FVF;

	m_dwIdxSize = sizeof(INDEX16);
	m_IdxFmt = D3DFMT_INDEX16;
	m_dwTriCnt = (dwVCX - 1) * (dwVCZ - 1) * 2;

	HRESULT		hr = CVIBuffer::CreateBuffer();
	FAILED_CHECK(hr);

	SetBuffers();

	return S_OK;
}

void Engine::CLandColor::SetBuffers(void)
{
	for(int iv = 0; iv < 2; ++iv)
	{
		// 버텍스 설정
		{
			Engine::VTXCOL* pVtxCol = NULL;
			m_pVB[iv]->Lock(0, 0, (void**)&pVtxCol, 0);

			for (DWORD i = 0; i < m_dwVtxCnt; ++i)
			{
				int iX = static_cast<int>(m_dwVCX);
				int iZ = static_cast<int>(m_dwVCZ);

				float fX = static_cast<float>(((i % iX) * 2)) - static_cast<float>(iX);
				float fZ = static_cast<float>(iZ) - static_cast<float>(((i / iZ) * 2));

				// Y축 높이, RGB 색상 랜덤
				int iRand = (rand() % 3);

				switch (iRand)
				{
				case 0:
					SetVertex(pVtxCol, i, fX, -1.f, fZ, 255, 0, 0);
					break;
				case 1:
					SetVertex(pVtxCol, i, fX, 0.f, fZ, 0, 255, 0);
					break;
				case 2:
					SetVertex(pVtxCol, i, fX, 1.f, fZ, 0, 0, 255);
					break;
				}
			}

			m_pVB[iv]->Unlock();
		}
	}
	

	
	// 인덱스 설정
	{
		Engine::INDEX16* pIndex = NULL;
		m_pIB->Lock(0, 0, (void**)&pIndex, 0);

		for (DWORD i = 0, j = 0, k = 0; i < m_dwTriCnt/2; ++i, ++j)
		{
			if (0 != j && 0 == j % ((m_dwVCX-1)*2))
				++k;

			SetIndex(pIndex, j, i + k, i + k + 1, i + k + 1 + (m_dwVCX));
			++j;
			SetIndex(pIndex, j, i + k, i + k + 1 + (m_dwVCX), i + k + (m_dwVCX));
		}

		m_pIB->Unlock();
	}
}

void Engine::CLandColor::SetVertex(Engine::VTXCOL * pVertex, int iIndex, float fx, float fy, float fz, DWORD dwRed, DWORD dwGreen, DWORD dwBlue)
{
	pVertex[iIndex].vPos = D3DXVECTOR3(fx, fy, fz);
	pVertex[iIndex].dwColor = D3DCOLOR_ARGB(255, dwRed, dwGreen, dwBlue);
}

void Engine::CLandColor::SetIndex(Engine::INDEX16 * pIndex, int iIndex, int i_1, int i_2, int i_3)
{
	pIndex[iIndex]._1 = i_1;
	pIndex[iIndex]._2 = i_2;
	pIndex[iIndex]._3 = i_3;
}

Engine::CLandColor * Engine::CLandColor::Create(LPDIRECT3DDEVICE9 pDevice, DWORD dwVCX, DWORD dwVCZ)
{
	CLandColor* pBuffer = new CLandColor(pDevice);
	if (FAILED(pBuffer->CreateBuffer(dwVCX, dwVCZ)))
		Safe_Delete(pBuffer);

	return pBuffer;
}
