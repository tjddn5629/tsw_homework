#include "ResourceMgr.h"
#include "TriColor.h"
#include "RcColor.h"
#include "RcTex.h"
#include "LandColor.h"
#include "LandTex.h"
#include "Texture.h"

IMPLEMENT_SINGLETON(Engine::CResourceMgr)

Engine::CResourceMgr::CResourceMgr()
{
}

Engine::CResourceMgr::~CResourceMgr()
{
	Release();
}

Engine::CComponent* Engine::CResourceMgr::CloneResource(RESOURCETYPE eResourceID, const wstring & wstrResourceKey)
{
	MAPRESOURCE::iterator	iter = m_mapResource[eResourceID].find(wstrResourceKey);
	if (iter == m_mapResource[eResourceID].end())
	{
		wstring	wstrErr = wstrResourceKey + L"리소스 복사 실패";
		MSG_BOX(wstrErr.c_str());
		return NULL;
	}
	return iter->second->CloneResource();
}

HRESULT Engine::CResourceMgr::AddBuffer(LPDIRECT3DDEVICE9 pDevice
	, RESOURCETYPE eResourceID
	, BUFFERTYPE eBufferID
	, const wstring wstrResourceKey
	, const WORD& wCntX /*= 0*/
	, const WORD& wCntZ /*= 0*/
	, const WORD& wItv /*= 1*/)
{
	MAPRESOURCE::iterator	iter = m_mapResource[eResourceID].find(wstrResourceKey);
	if (iter != m_mapResource[eResourceID].end())
	{
		MSG_BOX(wstrResourceKey.c_str());
		return S_OK;
	}

	CResources* pResource = NULL;
	switch (eBufferID)
	{
	case BUFFER_TRICOLOR:
		pResource = CTriColor::Create(pDevice);
		break;

	case BUFFER_RCCOLOR:
		pResource = CRcColor::Create(pDevice);
		break;

	case BUFFER_RCTEX:
		pResource = CRcTex::Create(pDevice);
		break;
		
	case BUFFER_LANDCOLOR:
		pResource = CLandColor::Create(pDevice, wCntX, wCntZ);
		break;

	case BUFFER_LANDTEX:
		pResource = CLandTex::Create(pDevice, wCntX, wCntZ, wItv);
		break;
	}
	NULL_CHECK_RETURN(pResource, E_FAIL);
	m_mapResource[eResourceID].insert(MAPRESOURCE::value_type(wstrResourceKey, pResource));

	return S_OK;
}

HRESULT Engine::CResourceMgr::AddTexture(LPDIRECT3DDEVICE9 pDevice, RESOURCETYPE eResourceID, TEXTURETYPE eTextureID, const wstring wstrResourceKey, const wstring & wstrFilePath, const WORD & wCnt)
{
	MAPRESOURCE::iterator	iter = m_mapResource[eResourceID].find(wstrResourceKey);
	if (iter != m_mapResource[eResourceID].end())
		return E_FAIL;

	CResources* pResource = CTexture::Create(pDevice, eTextureID, wstrFilePath, wCnt);
	NULL_CHECK_RETURN_MSG(pResource, E_FAIL, wstrFilePath.c_str());

	m_mapResource[eResourceID].insert(MAPRESOURCE::value_type(wstrResourceKey, pResource));
	return S_OK;
}

void Engine::CResourceMgr::ResetDynamic(void)
{
	for_each(m_mapResource[RESOURCE_DYNAMIC].begin(), m_mapResource[RESOURCE_DYNAMIC].end(), CDeleteMap());
	m_mapResource[RESOURCE_DYNAMIC].clear();
}

void Engine::CResourceMgr::Release(void)
{
	for (int i = 0; i < RESOURCE_END; ++i)
	{
		for_each(m_mapResource[i].begin(), m_mapResource[i].end(), CDeleteMap());
		m_mapResource[i].clear();
	}
}

const Engine::CResources* Engine::CResourceMgr::GetBuffer(RESOURCETYPE eResourceID, const wstring& wstrResourceKey)
{
	MAPRESOURCE::iterator	iter = m_mapResource[eResourceID].find(wstrResourceKey);
	if (iter == m_mapResource[eResourceID].end())
		return NULL;

	return iter->second;
}

