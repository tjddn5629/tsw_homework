#ifndef VIBuffer_h__
#define VIBuffer_h__

#include "Resources.h"

BEGIN(Engine)

class CObserver;
class ENGINE_DLL CVIBuffer :
	public CResources
{
protected:
	explicit CVIBuffer(LPDIRECT3DDEVICE9 pDevice);

public:
	virtual ~CVIBuffer();

public:
	virtual CResources* CloneResource(void) PURE;

public:
	virtual HRESULT CreateBuffer(void);

private:
	void SetRenderingPipeline(void);

public:
	void	SetRenderMatrix(D3DXMATRIX* pMat) { m_pMat = pMat; }

public:
	virtual void Render(void);

protected:
	void Release(void);

protected:
	LPDIRECT3DVERTEXBUFFER9		m_pVB[2];
	DWORD						m_dwVtxSize;
	DWORD						m_dwVtxCnt;
	DWORD						m_dwVtxFVF;

protected:
	LPDIRECT3DINDEXBUFFER9		m_pIB;
	DWORD						m_dwIdxSize;
	DWORD						m_dwTriCnt;
	D3DFORMAT					m_IdxFmt;

protected:
	D3DXMATRIX*					m_pMat;
};

END

#endif // !VIBuffer_h__



