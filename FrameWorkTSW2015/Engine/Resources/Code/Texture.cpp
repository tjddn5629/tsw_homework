#include "Texture.h"

Engine::CTexture::CTexture(LPDIRECT3DDEVICE9 pGraphicDev)
	: CResources(pGraphicDev)
{

}

Engine::CTexture::~CTexture(void)
{
	Release();
}

Engine::CResources* Engine::CTexture::CloneResource(void)
{
	CResources* pResources = new CTexture(*this);

	++(*m_pwRefCnt);

	return pResources;
}

HRESULT Engine::CTexture::LoadTexture(TEXTURETYPE eTextureType, const wstring& wstrFilePath, const WORD& wCnt)
{
	IDirect3DBaseTexture9*		pTexture = NULL;

	if (wCnt == 0)
		return E_FAIL;

	m_vecTexture.reserve(wCnt);
	TCHAR	szFullPath[_MAX_DIR] = TEXT("");
	
	for (size_t i = 0; i < wCnt; ++i)
	{
		wsprintf(szFullPath, wstrFilePath.c_str(), i);

		TEX_INFO* pTexInfo = new TEX_INFO;
		ZeroMemory(pTexInfo, sizeof(TEX_INFO));

		if (FAILED(D3DXGetImageInfoFromFile(szFullPath, &pTexInfo->ImageInfo)))
		{
			MSG_BOX(TEXT("이미지 정보 읽기 실패"));
			delete pTexInfo;
			return E_FAIL;
		}

		HRESULT hr = NULL;

		switch (eTextureType)
		{
		case TEXTURE_NORMAL:
			hr = D3DXCreateTextureFromFileEx(m_pDevice
				, szFullPath
				, pTexInfo->ImageInfo.Width
				, pTexInfo->ImageInfo.Height
				, pTexInfo->ImageInfo.MipLevels
				, 0
				, pTexInfo->ImageInfo.Format
				, D3DPOOL_MANAGED
				, D3DX_DEFAULT
				, D3DX_DEFAULT
				, NULL		// 제거할 컬러 키
				, &pTexInfo->ImageInfo
				, NULL
				, (LPDIRECT3DTEXTURE9*)&pTexInfo->pTexture);
			break;
		case TEXTURE_CUBE:
			hr = D3DXCreateCubeTextureFromFileEx(m_pDevice
				, szFullPath
				, D3DX_DEFAULT
				, pTexInfo->ImageInfo.MipLevels
				, 0
				, pTexInfo->ImageInfo.Format
				, D3DPOOL_MANAGED
				, D3DX_DEFAULT
				, D3DX_DEFAULT
				, NULL		// 제거할 컬러 키
				, &pTexInfo->ImageInfo
				, NULL
				, (LPDIRECT3DCUBETEXTURE9*)&pTexInfo->pTexture);
			break;
		}
		FAILED_CHECK_MSG(hr, szFullPath);

		m_vecTexture.push_back(pTexInfo);
	}

	m_dwContainerSize = m_vecTexture.size();
	return S_OK;
}

void Engine::CTexture::Render(const DWORD& dwStage, const DWORD& iIndex)
{
	if (iIndex >= m_dwContainerSize)
		return;

	m_pDevice->SetTexture(dwStage, m_vecTexture[iIndex]->pTexture);
}

Engine::CTexture* Engine::CTexture::Create(LPDIRECT3DDEVICE9 pGraphicDev, TEXTURETYPE eTextureType
	, const wstring& wstrFilePath, const WORD& wCnt)
{
	CTexture*	pTexture = new CTexture(pGraphicDev);
	if (FAILED(pTexture->LoadTexture(eTextureType, wstrFilePath, wCnt)))
		Engine::Safe_Delete(pTexture);

	return pTexture;
}

void Engine::CTexture::Release(void)
{
	if ((*m_pwRefCnt) == 0)
	{
		DWORD		dwSize = m_vecTexture.size();
		for (DWORD i = 0; i < dwSize; ++i)
		{
			m_vecTexture[i]->pTexture->Release();
			Engine::Safe_Delete(m_vecTexture[i]);
		}
		m_vecTexture.clear();
	}
}