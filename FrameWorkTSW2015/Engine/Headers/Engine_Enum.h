/*!
 * \file Engine_Enum.h
 * \date 2015/05/01 15:57
 *
 * \author Administrator
 * Contact: user@company.com
 *
 * \brief 
 *
 * TODO: long description
 *
 * \note
*/

#ifndef Engine_Enum_h__
#define Engine_Enum_h__

namespace Engine
{
	//! @ brief 씬이 전환될때 지워야할 리소스와 유지해야할 리소스로 나눈다.
	enum RESOURCETYPE
	{
		RESOURCE_STATIC,	//!< 유지해야 할 리소스
		RESOURCE_DYNAMIC,	//!< 지워야 할 리소스
		RESOURCE_END,
	};

	enum TEXTURETYPE
	{
		TEXTURE_NORMAL,
		TEXTURE_CUBE,
		TEXTURE_END,
	};

	enum BUFFERTYPE
	{
		BUFFER_TRICOLOR,
		BUFFER_RCCOLOR,
		BUFFER_RCTEX,
		BUFFER_LANDCOLOR,
		BUFFER_LANDTEX,
	};

	enum ANGLE {ANGLE_X, ANGLE_Y, ANGLE_Z, ANGLE_END,};

	// 씬 타입
	enum eSCENE_TYPE {
		SCENE_LOGO,			// 로고
		SCENE_STAGE,		// 인게임
		SCENE_END
	};

	// 키 눌림용 
	enum eKEYID {
		KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9, KEY_0,
		KEY_Q, KEY_W, KEY_W_DOWN, KEY_W_RELEASE, KEY_E, KEY_R, KEY_T, KEY_Y, KEY_U, KEY_I, KEY_I_RELEASE, KEY_O, KEY_P,
		KEY_A, KEY_A_DOWN, KEY_A_RELEASE, KEY_S, KEY_S_DOWN, KEY_S_RELEASE, KEY_D, KEY_D_DOWN, KEY_D_RELEASE, KEY_F, KEY_G, KEY_H, KEY_J, KEY_K, KEY_L, KEY_L_RELEASE,
		KEY_Z, KEY_X, KEY_C, KEY_V, KEY_B, KEY_N, KEY_M,
		KEY_LBUTTON, KEY_LBUTTON_RELEASE, KEY_LBUTTON_DRAG,
		KEY_RBUTTON, KEY_RBUTTON_RELEASE, KEY_RBUTTON_DRAG,
		KEY_BACKSPACE, KEY_ENTER,
		KEY_LSHIFT, KEY_LSHIFT_DOWN, KEY_LSHIFT_RELEASE, KEY_RSHIFT, KEY_LCONTROL, KEY_RCONTROL, KEY_ALT,
		KEY_ADD, KEY_MIN,
		KEY_RIGHT, KEY_LEFT, KEY_UP, KEY_DOWN,
		KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6,
		KEY_F7, KEY_F8, KEY_F9, KEY_F10, KEY_F11, KEY_F12,
		KEY_END
	};
}

#endif // Engine_Enum_h__