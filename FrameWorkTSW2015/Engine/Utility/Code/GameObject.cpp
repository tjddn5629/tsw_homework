#include "GameObject.h"
#include "Component.h"


Engine::CGameObject::CGameObject(LPDIRECT3DDEVICE9 pDevice)
:m_pDevice(pDevice)
{
}


Engine::CGameObject::~CGameObject()
{
	Release();
}

void Engine::CGameObject::Update(void)
{
	// 컴포넌트 업데이트
	{
		MAPCOMPONENT::iterator iter = m_mapComponent.begin();
		MAPCOMPONENT::iterator iter_end = m_mapComponent.end();

		for (; iter != iter_end; ++iter)
			iter->second->Update();
	}
}

void Engine::CGameObject::Render(void)
{
}

void Engine::CGameObject::Release(void)
{
	for_each(m_mapComponent.begin(), m_mapComponent.end(), CDeleteMap());
	m_mapComponent.clear();
}
