#ifndef __KEYMGR_H__
#define __KEYMGR_H__
#include "Engine_Include.h"

BEGIN(Engine)

class ENGINE_DLL CKeyMgr
{
public:
	DECLARE_SINGLETON(CKeyMgr)

private:
	CKeyMgr(void);
	~CKeyMgr(void);

public:
	bool GetKeyState(eKEYID eKey) { return m_KeyDown[eKey]; }

public:
	void SetKeyState(void);

private:
	bool			m_KeyDown[KEY_END];
	bool			m_KeyBefor[KEY_END];
	SHORT			m_KeyBuf[KEY_END];
	SHORT			m_KeyBufBefor[KEY_END];
};
END

#endif