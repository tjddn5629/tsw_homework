#include "SceneMgr.h"
#include "Scene.h"

IMPLEMENT_SINGLETON(Engine::CSceneMgr)

void Engine::CSceneMgr::Initialize(void)
{
	m_pScene = NULL;
	m_bChangeScene = false;
	m_eType = SCENE_END;
	m_eTypeBefore = SCENE_END;
}

void Engine::CSceneMgr::Update(void)
{
	if (m_pScene)
		m_pScene->Update();		
}

void Engine::CSceneMgr::Render()
{
	if (m_pScene)
		m_pScene->Render();
}

void Engine::CSceneMgr::Release(void)
{
	Safe_Delete(m_pScene);
}

void Engine::CSceneMgr::SetScene(eSCENE_TYPE eType, CScene * pScene)
{	
	if (m_pScene)
	{
		delete m_pScene;
		m_pScene = NULL;
	}

	m_eType = eType;
	m_pScene = pScene;	
}

void Engine::CSceneMgr::SetChangeScene(eSCENE_TYPE eType, int iIndex)
{
	m_eType = eType;
	m_bChangeScene = true;
}

Engine::CSceneMgr::CSceneMgr()
	:m_pScene(NULL)
{
}


Engine::CSceneMgr::~CSceneMgr()
{
	Release();
}
