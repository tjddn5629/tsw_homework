#include "InfoSubject.h"

IMPLEMENT_SINGLETON(Engine::CInfoSubject)

void Engine::CInfoSubject::AddData(int iMessage, void * pData)
{
	if (pData != NULL)
	{
		map<int, list<void*>*>::iterator iter;

		iter = m_mapData.find(iMessage);

		if (iter == m_mapData.end())
		{
			list<void*>* pTemp = new list<void*>();

			m_mapData.insert(make_pair(iMessage, pTemp));
		}

		m_mapData[iMessage]->push_back(pData);

		Notify(iMessage, pData);
	}
}

void Engine::CInfoSubject::RemoveData(int iMessage, void * pData)
{
	map<int, list<void*>*>::iterator iter;
	map<int, list<void*>*>::iterator iter_end;

	iter = m_mapData.find(iMessage);

	if (iter != m_mapData.end())
	{
		list<void*>* pList = m_mapData[iMessage];

		for (list<void*>::iterator Dataiter = pList->begin();
			Dataiter != pList->end(); )
		{
			if ((*Dataiter) == pData)
			{
				Dataiter = pList->erase(Dataiter);
			}
			else
				++Dataiter;
		}
	}
}

void Engine::CInfoSubject::Release(void)
{
	map<int, list<void*>*>::iterator iter;
	map<int, list<void*>*>::iterator iter_end;

	iter = m_mapData.begin();
	iter_end = m_mapData.end();

	for (iter; iter != iter_end; ++iter)
	{
		iter->second->clear();
		Safe_Delete(iter->second);
	}
	m_mapData.clear();
}

Engine::CInfoSubject::CInfoSubject()
{
}


Engine::CInfoSubject::~CInfoSubject()
{
	Release();
}
