#ifndef Observer_h__
#define Observer_h__

#include "Engine_Include.h"

BEGIN(Engine)
class ENGINE_DLL CObserver abstract
{
protected:
	CObserver(void);
public:
	virtual ~CObserver(void);

public:
	virtual void Update(int _iMessage, void* _pData)PURE;
};

END

#endif