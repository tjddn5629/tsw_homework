#include "KeyMgr.h"

IMPLEMENT_SINGLETON(Engine::CKeyMgr)

Engine::CKeyMgr::CKeyMgr(void)
{
	for(int i = 0; i < KEY_END; ++i)
	{
		m_KeyDown[i] = false;
	}
}

Engine::CKeyMgr::~CKeyMgr(void)
{
}

void Engine::CKeyMgr::SetKeyState(void)
{
	memcpy_s(m_KeyBufBefor, sizeof(SHORT)*KEY_END, m_KeyBuf, sizeof(SHORT)*KEY_END);
	memcpy_s(m_KeyBefor, sizeof(bool)*KEY_END, m_KeyDown, sizeof(bool)*KEY_END);
	memset(m_KeyDown, false, sizeof(bool)*KEY_END);

	// 숫자 0
	if(GetAsyncKeyState(0x30))
		m_KeyDown[KEY_0] = true;

	// 숫자 1
	if(GetAsyncKeyState(0x31))
		m_KeyDown[KEY_1] = true;

	// 숫자 2
	if(GetAsyncKeyState(0x32))
		m_KeyDown[KEY_2] = true;

	// 숫자 3
	if(GetAsyncKeyState(0x33))
		m_KeyDown[KEY_3] = true;

	// 숫자 4
	if(GetAsyncKeyState(0x34))
		m_KeyDown[KEY_4] = true;

	// 숫자 5
	if(GetAsyncKeyState(0x35))
		m_KeyDown[KEY_5] = true;

	// 숫자 6
	if(GetAsyncKeyState(0x36))
		m_KeyDown[KEY_6] = true;

	// 숫자 7
	if(GetAsyncKeyState(0x37))
		m_KeyDown[KEY_7] = true;

	// 숫자 8
	if(GetAsyncKeyState(0x38))
		m_KeyDown[KEY_8] = true;

	// 숫자 9
	if(GetAsyncKeyState(0x39))
		m_KeyDown[KEY_9] = true;

	////////////////////////////////////////////////////////////

	// Q
	if(GetAsyncKeyState('Q'))
		m_KeyDown[KEY_Q] = true;

	// W
	m_KeyBuf[KEY_W] = GetAsyncKeyState('W');
	if (!m_KeyBefor[KEY_W] && m_KeyBuf[KEY_W] == (SHORT)0x8001)
		m_KeyDown[KEY_W] = true;

	// W 눌려진 상태
	if (m_KeyBuf[KEY_W] == (SHORT)0x8000)
		m_KeyDown[KEY_W_DOWN] = true;

	// W 릴리즈
	if (m_KeyBefor[KEY_W] && !m_KeyBuf[KEY_W])
		m_KeyDown[KEY_W_RELEASE] = true;

	// E
	if(GetAsyncKeyState('E'))
		m_KeyDown[KEY_E] = true;

	// R
	if(GetAsyncKeyState('R'))
		m_KeyDown[KEY_R] = true;

	// T
	if(GetAsyncKeyState('T'))
		m_KeyDown[KEY_T] = true;

	// Y
	if(GetAsyncKeyState('Y'))
		m_KeyDown[KEY_Y] = true;

	// U
	if(GetAsyncKeyState('U'))
		m_KeyDown[KEY_U] = true;

	// I
	m_KeyBuf[KEY_I] = GetAsyncKeyState('I');
	if (!m_KeyBefor[KEY_I] && m_KeyBuf[KEY_I] == (SHORT)0x8001)
		m_KeyDown[KEY_I] = true;

	// I 릴리즈
	if (m_KeyBefor[KEY_I] && !m_KeyBuf[KEY_I])
		m_KeyDown[KEY_I_RELEASE] = true;

	// O
	if(GetAsyncKeyState('O'))
		m_KeyDown[KEY_O] = true;
	
	// P
	if(GetAsyncKeyState('P'))
		m_KeyDown[KEY_P] = true;
	
	// A
	m_KeyBuf[KEY_A] = GetAsyncKeyState('A');
	if (!m_KeyBefor[KEY_A] && m_KeyBuf[KEY_A] == (SHORT)0x8001)
		m_KeyDown[KEY_A] = true;

	// A 눌려진 상태
	if (m_KeyBuf[KEY_A] == (SHORT)0x8000)
		m_KeyDown[KEY_A_DOWN] = true;

	// A 릴리즈
	if (m_KeyBefor[KEY_A] && !m_KeyBuf[KEY_A])
		m_KeyDown[KEY_A_RELEASE] = true;

	// S
	m_KeyBuf[KEY_S] = GetAsyncKeyState('S');
	if (!m_KeyBefor[KEY_S] && m_KeyBuf[KEY_S] == (SHORT)0x8001)
		m_KeyDown[KEY_S] = true;

	// S 눌려진 상태
	if (m_KeyBuf[KEY_S] == (SHORT)0x8000)
		m_KeyDown[KEY_S_DOWN] = true;

	// S 릴리즈
	if (m_KeyBefor[KEY_S] && !m_KeyBuf[KEY_S])
		m_KeyDown[KEY_S_RELEASE] = true;

	// D
	m_KeyBuf[KEY_D] = GetAsyncKeyState('D');
	if (!m_KeyBefor[KEY_D] && m_KeyBuf[KEY_D] == (SHORT)0x8001)
		m_KeyDown[KEY_D] = true;

	// D 눌려진 상태
	if (m_KeyBuf[KEY_D] == (SHORT)0x8000)
		m_KeyDown[KEY_D_DOWN] = true;

	// D 릴리즈
	if (m_KeyBefor[KEY_D] && !m_KeyBuf[KEY_D])
		m_KeyDown[KEY_D_RELEASE] = true;

	// F
	if(GetAsyncKeyState('F'))
		m_KeyDown[KEY_F] = true;

	// G
	if(GetAsyncKeyState('G'))
		m_KeyDown[KEY_G] = true;

	// H
	if(GetAsyncKeyState('H'))
		m_KeyDown[KEY_H] = true;

	// J
	if(GetAsyncKeyState('J'))
		m_KeyDown[KEY_J] = true;

	// K
	if(GetAsyncKeyState('K'))
		m_KeyDown[KEY_K] = true;
	
	// L
	m_KeyBuf[KEY_L] = GetAsyncKeyState('L');
	if(!m_KeyBefor[KEY_L] && m_KeyBuf[KEY_L] == (SHORT)0x8001)
		m_KeyDown[KEY_L] = true;
	
	// L 릴리즈
	if (m_KeyBefor[KEY_L] && !m_KeyBuf[KEY_L])
		m_KeyDown[KEY_L_RELEASE] = true;

	// Z
	if(GetAsyncKeyState('Z'))
		m_KeyDown[KEY_Z] = true;

	// X
	if(GetAsyncKeyState('X'))
		m_KeyDown[KEY_X] = true;

	// C
	if(GetAsyncKeyState('C'))
		m_KeyDown[KEY_C] = true;

	// V
	if(GetAsyncKeyState('V'))
		m_KeyDown[KEY_V] = true;

	// B
	if(GetAsyncKeyState('B'))
		m_KeyDown[KEY_B] = true;

	// N
	if(GetAsyncKeyState('N'))
		m_KeyDown[KEY_N] = true;

	// M
	if(GetAsyncKeyState('M'))
		m_KeyDown[KEY_M] = true;

	// LSHIFT
	m_KeyBuf[KEY_LSHIFT] = GetAsyncKeyState(VK_LSHIFT);
	if (!m_KeyBefor[KEY_LSHIFT] && m_KeyBuf[KEY_LSHIFT] == (SHORT)0x8001)
		m_KeyDown[KEY_LSHIFT] = true;

	// LSHIFT 눌려진 상태
	if (m_KeyBuf[KEY_LSHIFT] == (SHORT)0x8000)
		m_KeyDown[KEY_LSHIFT_DOWN] = true;

	// LSHIFT 릴리즈
	if (m_KeyBefor[KEY_LSHIFT] && !m_KeyBuf[KEY_LSHIFT])
		m_KeyDown[KEY_LSHIFT_RELEASE] = true;

	// RSHIFT
	if(GetAsyncKeyState(VK_RSHIFT))
		m_KeyDown[KEY_RSHIFT] = true;

	// LCONTROL
	if(GetAsyncKeyState(VK_LCONTROL))
		m_KeyDown[KEY_LCONTROL] = true;
	
	// LCONTROL
	if(GetAsyncKeyState(VK_RCONTROL))
		m_KeyDown[KEY_RCONTROL] = true;

	// LCONTROL
	if(GetAsyncKeyState(VK_MENU))
		m_KeyDown[KEY_ALT] = true;

	// LEFT
	if(GetAsyncKeyState(VK_LEFT))
		m_KeyDown[KEY_LEFT] = true;

	// RIGHT
	if(GetAsyncKeyState(VK_RIGHT))
		m_KeyDown[KEY_RIGHT] = true;

	// DOWN
	if(GetAsyncKeyState(VK_DOWN))
		m_KeyDown[KEY_DOWN] = true;

	// UP
	if(GetAsyncKeyState(VK_UP))
		m_KeyDown[KEY_UP] = true;

	///////////////////////////////////////////////////////
	// 마우스 이벤트가 완벽한 커스터마이징( 다른거 필요하면 보고 할것)
	///////////////////////////////////////////////////////
	// 마우스 왼쪽 클릭
	m_KeyBuf[KEY_LBUTTON] = GetAsyncKeyState(VK_LBUTTON);
	// 이전에 클릭한적 없고, 현재 눌려진 상태라면
	if(m_KeyBufBefor[KEY_LBUTTON] == 0 && m_KeyBuf[KEY_LBUTTON] & (SHORT)0x8001)
		m_KeyDown[KEY_LBUTTON] = true;

	// 마우스 드래그
	// 현재 눌려진 상태라면
	if (m_KeyBuf[KEY_LBUTTON] == (SHORT)0x8000)
		m_KeyDown[KEY_LBUTTON_DRAG] = true;

	// 마우스 왼쪽 릴리즈
	if(m_KeyBefor[KEY_LBUTTON_DRAG] && !m_KeyBuf[KEY_LBUTTON])
		m_KeyDown[KEY_LBUTTON_RELEASE] = true;

	/////////////////////////////////////////////////////////////////////
	// 마우스 오른쪽 클릭
	m_KeyBuf[KEY_RBUTTON] = GetAsyncKeyState(VK_RBUTTON);	
	if(m_KeyBufBefor[KEY_RBUTTON] == 0 && m_KeyBuf[KEY_RBUTTON] & (SHORT)0x8001)
		m_KeyDown[KEY_RBUTTON] = true;

	// 마우스 오른쪽 드래그
	if(m_KeyBuf[KEY_RBUTTON] == (SHORT)0x8000)
		m_KeyDown[KEY_RBUTTON_DRAG] = true;
	
	// 마우스 오른쪽 릴리즈
	if (m_KeyBefor[KEY_RBUTTON_DRAG] && !m_KeyBuf[KEY_RBUTTON])
		m_KeyDown[KEY_RBUTTON_RELEASE] = true;
	//////////////////////////////////////////////////////////////
	// + 처음 눌렀을 때
	if( GetAsyncKeyState(VK_ADD) & (SHORT)0x8001)
		m_KeyDown[KEY_ADD] = true;

	// - 처음 눌렀을 때
	if( GetAsyncKeyState(VK_SUBTRACT) & (SHORT)0x8001)
		m_KeyDown[KEY_MIN] = true;

	// & 1은 처음 눌렀을때
	// backspace
	if(GetAsyncKeyState(VK_BACK) & 1)
		m_KeyDown[KEY_BACKSPACE] = true;

	// ENTER
	if(GetAsyncKeyState(VK_RETURN) & 1)
		m_KeyDown[KEY_ENTER] = true;

	// F1~F12
	if(GetAsyncKeyState(VK_F1) & 1)
		m_KeyDown[KEY_F1] = true;

	if(GetAsyncKeyState(VK_F2) & 1)
		m_KeyDown[KEY_F2] = true;

	if(GetAsyncKeyState(VK_F3) & 1)
		m_KeyDown[KEY_F3] = true;

	if(GetAsyncKeyState(VK_F4) & 1)
		m_KeyDown[KEY_F4] = true;

	if(GetAsyncKeyState(VK_F5) & 1)
		m_KeyDown[KEY_F5] = true;

	if(GetAsyncKeyState(VK_F6) & 1)
		m_KeyDown[KEY_F6] = true;

	if(GetAsyncKeyState(VK_F7) & 1)
		m_KeyDown[KEY_F7] = true;

	if(GetAsyncKeyState(VK_F8) & 1)
		m_KeyDown[KEY_F8] = true;

	if(GetAsyncKeyState(VK_F9) & 1)
		m_KeyDown[KEY_F9] = true;

	if(GetAsyncKeyState(VK_F10) & 1)
		m_KeyDown[KEY_F10] = true;

	if(GetAsyncKeyState(VK_F11) & 1)
		m_KeyDown[KEY_F11] = true;

	if(GetAsyncKeyState(VK_F12) & 1)
		m_KeyDown[KEY_F12] = true;
}