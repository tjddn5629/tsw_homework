#include "Pipeline.h"
#include "MathMgr.h"

Engine::CPipeline::CPipeline()
{
}


Engine::CPipeline::~CPipeline()
{
}

void Engine::CPipeline::MakeWorldMatrix(D3DXMATRIX * pOut, const D3DXVECTOR3 * pScale, const float * pAngle, const D3DXVECTOR3 * pPos)
{
	D3DXMatrixIdentity(pOut);

	D3DXVECTOR3		vRight	= D3DXVECTOR3(1.f, 0.f, 0.f); //1행
	D3DXVECTOR3		vUp		= D3DXVECTOR3(0.f, 1.f, 0.f); //2행
	D3DXVECTOR3		vLook	= D3DXVECTOR3(0.f, 0.f, 1.f); //3행
	D3DXVECTOR3		vPos	= D3DXVECTOR3(0.f, 0.f, 0.f); //4행

	// 스케일 -> 자전 -> 이동 -> 공전 -> 부모
	// 1. 스케일
	vRight	*= pScale->x;
	vUp		*= pScale->y;
	vLook	*= pScale->z;

	// 자전 x -> Y-> Z
	// X축 기준으로 회전
	//CMathMgr::MyRotationX(&vRight, &vRight, pAngle[ANGLE_X]);
	CMathMgr::MyRotationX(&vUp, &vUp, pAngle[ANGLE_X]);
	CMathMgr::MyRotationX(&vLook, &vLook, pAngle[ANGLE_X]);

	// Y축 기준으로 회전
	CMathMgr::MyRotationY(&vRight, &vRight, pAngle[ANGLE_Y]);
	//CMathMgr::MyRotationY(&vUp, &vUp, pAngle[ANGLE_X]);
	CMathMgr::MyRotationY(&vLook, &vLook, pAngle[ANGLE_Y]);

	// Z축 기준으로 회전
	CMathMgr::MyRotationZ(&vRight, &vRight, pAngle[ANGLE_Z]);
	CMathMgr::MyRotationZ(&vUp, &vUp, pAngle[ANGLE_Z]);
	//CMathMgr::MyRotationZ(&vLook, &vLook, pAngle[ANGLE_X]);

	vPos = *pPos;

	MakeTransformMatrix(pOut, &vRight, &vUp, &vLook, &vPos);
}

void Engine::CPipeline::MakeTransformMatrix(D3DXMATRIX * pOut, const D3DXVECTOR3 * pRight, const D3DXVECTOR3 * pUp, const D3DXVECTOR3 * pLook, const D3DXVECTOR3 * pPos)
{
	memcpy(&pOut->m[0][0], pRight, sizeof(D3DXVECTOR3));
	memcpy(&pOut->m[1][0], pUp, sizeof(D3DXVECTOR3));
	memcpy(&pOut->m[2][0], pLook, sizeof(D3DXVECTOR3));
	memcpy(&pOut->m[3][0], pPos, sizeof(D3DXVECTOR3));
}

void Engine::CPipeline::MakeProjectionMatrix(D3DXMATRIX * pOut, const float & fFovY, const float & fAspect, const float & fNear, const float & fFar)
{
	D3DXMatrixIdentity(pOut);

	// fAspect: 화면의 가로세로 비율
	pOut->_11 = (1.f / tanf(fFovY / 2.f)) / fAspect;
	pOut->_22 = 1.f / tanf(fFovY / 2.f);

	pOut->_33 = fFar / (fFar - fNear);
	pOut->_43 = -fNear * fFar / (fFar - fNear);

	pOut->_34 = 1.f;
	pOut->_44 = 0.f;
}

void Engine::CPipeline::MakeViewMatrix(D3DXMATRIX * pOut, const D3DXVECTOR3 * pPos, const D3DXVECTOR3 * pTarget, const D3DXVECTOR3 * pUp)
{
	D3DXMatrixIdentity(pOut);

	D3DXVECTOR3		vRight = D3DXVECTOR3(1.f, 0.f, 0.f);	//1행
	D3DXVECTOR3		vUp = *pUp;								//2행
	D3DXVECTOR3		vLook = D3DXVECTOR3(0.f, 0.f, 1.f);		//3행
	D3DXVECTOR3		vPos = D3DXVECTOR3(0.f, 0.f, 0.f);		//4행
	
	// 축계산
	{
		vLook = *pTarget - *pPos;
		D3DXVec3Normalize(&vLook, &vLook);		// 카메라가 바라보는 방향  Z 축

		D3DXVec3Cross(&vRight, &vUp, &vLook);
		D3DXVec3Normalize(&vRight, &vRight);	// 카메라의 X 축

		D3DXVec3Cross(&vUp, &vLook, &vRight);
		D3DXVec3Normalize(&vUp, &vUp);			// 카메라의 Y 축
	}

	//D3DXVECTOR3		vDot;	// 플레이어위치 기준 카메라의 위치 벡터
	//vDot.x = -D3DXVec3Dot(pPos, &vRight);
	//vDot.y = -D3DXVec3Dot(pPos, &vUp);
	//vDot.z = -D3DXVec3Dot(pPos, &vLook);

	//// 뷰 매트릭스 계산
	//memcpy(&pOut->m[0][0], &vRight, sizeof(D3DXVECTOR3));
	//memcpy(&pOut->m[1][0], &vUp, sizeof(D3DXVECTOR3));
	//memcpy(&pOut->m[2][0], &vLook, sizeof(D3DXVECTOR3));

	//D3DXMatrixTranspose(pOut, pOut);

	//memcpy(&pOut->m[3][0], &vDot, sizeof(D3DXVECTOR3));

	vPos = *pPos;

	MakeTransformMatrix(pOut, &vRight, &vUp, &vLook, &vPos);
	D3DXMatrixInverse(pOut, NULL, pOut);
}
