#ifndef Export_System_h__
#define Export_System_h__

#include "../../Reference/Headers/GraphicDev.h"
#include "../../Reference/Headers/TimeMgr.h"

BEGIN(Engine)

inline CGraphicDev* Get_GraphicDev(void);
inline CTimeMgr* Get_TimeMgr(void);

#include "Export_System.inl"

END

#endif // !Export_System_h__
