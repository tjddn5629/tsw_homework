#ifndef Define_h__
#define Define_h__

#define D_CAST(ClassName, Obj) dynamic_cast<ClassName*>(Obj)
#define S_CAST(Type, Obj)      static_cast<Type>(Obj)

#define S_CAST_INT(Obj)			static_cast<int>(Obj)
#define S_CAST_FLOAT(Obj)		static_cast<float>(Obj)

#endif