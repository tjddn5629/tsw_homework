
#ifndef Value_h__
#define Value_h__

const WORD		WINCX = 800;
const WORD		WINCY = 600;

const WORD		VTXCNTX = 129;
const WORD		VTXCNTZ = 129;
const WORD		VTXITV = 1;

const D3DXVECTOR3		g_vLook = D3DXVECTOR3(0.f, 0.f, 1.f);

#endif // Value_h__