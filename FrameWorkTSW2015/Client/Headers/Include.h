#ifndef Include_h__
#define Include_h__

#include "Enum.h"
#include "Value.h"
#include "Struct.h"
#include "Define.h"

extern HWND		g_hWnd;

extern const D3DXVECTOR3 g_vLook;

#endif // Include_h__