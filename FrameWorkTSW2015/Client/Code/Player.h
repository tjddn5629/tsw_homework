
#ifndef Player_h__
#define Player_h__

#include "GameObject.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTransform;
}

class CPlayer :
	public Engine::CGameObject
{
private:
	explicit CPlayer(LPDIRECT3DDEVICE9 pDevice);

public:
	virtual ~CPlayer(void);

public:
	virtual void Update(void);
	virtual void Render(void);

	Engine::CTransform* GetInfo(void);

public:
	static CPlayer* Create(LPDIRECT3DDEVICE9 pDevice);

private:
	HRESULT Initialize(void);
	HRESULT AddComponent(void);
	void Release(void);
	void KeyCheck(void);

private:
	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;
	Engine::CTransform*		m_pInfo;

private:
	bool					m_bChangeInfo;

private:
	D3DXMATRIX				m_matRocal;
};

#endif