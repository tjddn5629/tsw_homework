#include "stdafx.h"
#include "Player.h"

#include "Include.h"
#include "Export_Function.h"
#include "VIBuffer.h"
#include "Texture.h"
#include "Transform.h"

CPlayer::CPlayer(LPDIRECT3DDEVICE9 pDevice)
: Engine::CGameObject(pDevice)
, m_pBuffer(NULL)
, m_pTexture(NULL)
, m_pInfo(NULL)
, m_bChangeInfo(false)
{
}

CPlayer::~CPlayer()
{
	Release();
}

HRESULT CPlayer::Initialize(void)
{
	HRESULT		hr = NULL;

	hr = AddComponent();
	FAILED_CHECK(hr);

	return S_OK;
}

void CPlayer::Update(void)
{
	m_bChangeInfo = false;
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_matRocal);

	KeyCheck();

	// 오브젝트에 속해있는 컴포넌트들 업데이트용
	Engine::CGameObject::Update();

	// 상태가 변경 되었을 때만 매트릭스 갱신
	//if (m_bChangeInfo)
	m_matRocal = m_pInfo->m_matWorld;
}

void CPlayer::Render(void)
{
	m_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);	// 컬링을 안하도록 설정
	//m_pDevice->SetTransform(D3DTS_WORLD, &m_pInfo->m_matWorld);
	Engine::Get_SubjectMgr()->Notify(MESSAGE_OBJECT_MATRIX, &m_pInfo->m_matWorld);

	m_pTexture->Render(0, 0);
	m_pBuffer->Render();
	m_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);		// 컬링을 다시 시작 CCW:시계방향만 보이도록(디폴트값)
																// CW : 반시계방향만 보이도록
}

Engine::CTransform* CPlayer::GetInfo(void)
{
	return m_pInfo;
}

CPlayer * CPlayer::Create(LPDIRECT3DDEVICE9 pDevice)
{
	CPlayer*		pObj = new CPlayer(pDevice);
	if (FAILED(pObj->Initialize()))
	{
		Engine::Safe_Delete(pObj);
	}

	return pObj;
}

HRESULT CPlayer::AddComponent(void)
{
	Engine::CComponent*		pCompontent = NULL;
	pCompontent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.insert(MAPCOMPONENT::value_type(L"Transform", pCompontent));

	pCompontent = Engine::Get_ResourceMgr()->CloneResource(Engine::RESOURCE_STATIC, L"Buffer_RcTex");
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pCompontent);
	m_pBuffer->SetRenderMatrix(&(m_pInfo->m_matWorld));
	NULL_CHECK_RETURN(pCompontent, E_FAIL);
	m_mapComponent.insert(MAPCOMPONENT::value_type(L"Buffer", pCompontent));

	pCompontent = Engine::Get_ResourceMgr()->CloneResource(Engine::RESOURCE_DYNAMIC, L"Texture_Player");
	m_pTexture = dynamic_cast<Engine::CTexture*>(pCompontent);
	NULL_CHECK_RETURN(pCompontent, E_FAIL);
	m_mapComponent.insert(MAPCOMPONENT::value_type(L"Texture", pCompontent));

	m_matRocal = m_pInfo->m_matWorld;
	Engine::Get_SubjectMgr()->AddData(MESSAGE_OBJECT_MATRIX, &m_pInfo->m_matWorld);
	
	return S_OK;
}

void CPlayer::Release(void)
{
}

void CPlayer::KeyCheck(void)
{
	float	fTime = Engine::Get_TimeMgr()->GetTime();

	if (GetAsyncKeyState('W'))
	{
		m_pInfo->m_vPos += m_pInfo->m_vDir * 10.f * fTime;
		m_bChangeInfo = true;
	}

	if (GetAsyncKeyState('S'))
	{
		m_pInfo->m_vPos += -m_pInfo->m_vDir * 10.f * fTime;
		m_bChangeInfo = true;
	}

	if (GetAsyncKeyState('A'))
	{
		m_pInfo->m_fAnlge[Engine::ANGLE_Y] -= D3DXToRadian(90.f) * fTime;
		m_bChangeInfo = true;
	}

	if (GetAsyncKeyState('D'))
	{
		m_pInfo->m_fAnlge[Engine::ANGLE_Y] += D3DXToRadian(90.f) * fTime;
		m_bChangeInfo = true;
	}
}
