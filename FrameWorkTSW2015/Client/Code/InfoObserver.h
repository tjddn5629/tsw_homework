#ifndef InfoObserver_h__
#define InfoObserver_h__

#include "Observer.h"

class CInfoObserver
	: public Engine::CObserver
{
private:
	D3DXMATRIX			m_matObjectMatrix;
	D3DXMATRIX			m_matViewMatrix;
	D3DXMATRIX			m_matProjMatrix;

public:
	/*virtual D3DXMATRIX GetPipelineMatrix(void) { return m_matObjectMatrix*m_matViewMatrix*m_matProjMatrix; }
	virtual D3DXMATRIX GetObjMatrix(void) { return m_matObjectMatrix; }
	virtual D3DXMATRIX GetViewMatrix(void) { return m_matViewMatrix; }
	virtual D3DXMATRIX GetProjMatrix(void) {return m_matProjMatrix; }*/

public:
	virtual void Update(int _iMessage, void* _pData);
	
public:
	CInfoObserver();
	virtual ~CInfoObserver();
};

#endif