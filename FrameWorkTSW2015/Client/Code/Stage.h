#ifndef Stage_h__
#define Stage_h__

#include "Scene.h"

namespace Engine
{
	class CObserver;
}

class CPlayer;
class CStage
	: public Engine::CScene
{
public:
	enum LAYERID { LAYER_ENVIROMENT, LAYER_GAMELOGIC, LAYER_UI };

private:
	explicit CStage(LPDIRECT3DDEVICE9 pDevice);

public:
	virtual ~CStage(void);

public:
	virtual void Update(void);
	virtual void Render(void);

public:
	static CStage* Create(LPDIRECT3DDEVICE9 pDevice);

private:
	HRESULT	Initialize(void);
	HRESULT Add_Environment_Layer(void);
	HRESULT Add_GameLogic_Layer(void);
	HRESULT Add_UI_Layer(void);
	void Release(void);

private:
	CPlayer*	m_pPlayer;
	Engine::CObserver*		m_pObserver;
};


#endif // Stage_h__