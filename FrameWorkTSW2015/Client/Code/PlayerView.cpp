#include "stdafx.h"
#include "PlayerView.h"

#include "Include.h"
#include "Export_Function.h"
#include "Engine_Macro.h"
#include "Transform.h"
#include "Player.h"
#include "Pipeline.h"
#include "MathMgr.h"

CPlayerView::CPlayerView(LPDIRECT3DDEVICE9 pDevice)
: Engine::CGameObject(pDevice)
, m_pInfo(NULL)
, m_pPlayer(NULL)
, m_bChangeInfo(false)
{
}

CPlayerView::~CPlayerView()
{
	Release();
}

HRESULT CPlayerView::Initialize(void)
{
	HRESULT		hr = NULL;

	//------------------------------------------------------------
	m_vViewPos = D3DXVECTOR3(0.f, 20.f, -50.f);	
	m_vViewTarget = D3DXVECTOR3(0.f, 0.f, 0.f);
	m_fFovY = 45.f;

	m_vViewLook = m_vViewPos - m_vViewTarget;
	SetView(&m_vViewPos, &m_vViewTarget, D3DXVECTOR3(0.f, 1.f, 0.f));
	SetProj(1.f, 1000.f);
	//------------------------------------------------------------

	hr = AddComponent();
	FAILED_CHECK(hr);

	return S_OK;
}

void CPlayerView::Update(void)
{
	m_bChangeInfo = false;

	// 카메라의 타겟은 항상 플레이어
	m_vViewTarget = m_pPlayer->GetInfo()->m_vPos;
	
	// 공전					
	D3DXMATRIX	matRotPlayer, matTransPlayer, matRotX;
	D3DXVECTOR3 vTemp;
		
	KeyCheck();

	// 플레이어 자전에 따른 카메라 공전
	{
		D3DXVECTOR3		vRight = D3DXVECTOR3(1.f, 0.f, 0.f); //1행
		D3DXVECTOR3		vUp = D3DXVECTOR3(0.f, 1.f, 0.f); //2행
		D3DXVECTOR3		vLook = D3DXVECTOR3(0.f, 0.f, 1.f); //3행

		// 카메라가 타겟을 바라보는방향으로 Look벡터 초기화
		vLook = m_pPlayer->GetInfo()->m_vPos - m_pInfo->m_vPos;
		D3DXVec3Normalize(&vLook, &vLook);	// 길이를 1로

		// 룩벡터와 절대축 Y간에 외적으로  로컬 X축(Right) 벡터 구하기
		D3DXVec3Cross(&vRight, &vUp, &vLook);
		D3DXVec3Normalize(&vRight, &vRight);

		D3DXVec3Cross(&vUp, &vLook, &vRight);
		D3DXVec3Normalize(&vUp, &vUp);

		// 키 입력을 통해 X축 기준으로 회전한 각을 이용하여
		// 위에서 구한 각으로 x축 회전과, y축 회전된 각 로컬축을 계산
		float m_fAngleX = m_pInfo->m_fAnlge[Engine::ANGLE_X];
		Engine::CMathMgr::MyRotationX(&vUp, &vUp, m_fAngleX);
		Engine::CMathMgr::MyRotationX(&vLook, &vLook, m_fAngleX);
		
		float fAngleY = m_pPlayer->GetInfo()->m_fAnlge[Engine::ANGLE_Y];
		fAngleY += m_pInfo->m_fAnlge[Engine::ANGLE_Y];
		Engine::CMathMgr::MyRotationY(&vRight, &vRight, fAngleY);
		Engine::CMathMgr::MyRotationY(&vLook, &vLook, fAngleY);		

		// 계산된 로컬 축으로 매트릭스 계산, Y축은 절대축으로 계산
		Engine::CPipeline::MakeTransformMatrix(&m_pInfo->m_matWorld, &vRight, &D3DXVECTOR3(0.f, 1.f, 0.f), &vLook, &VEC3);
		
		// 회전 결과가 저장된 매트릭스를 통해 원본 룩벡터를 회전
		D3DXVec3TransformNormal(&vTemp, &m_vViewLook, &m_pInfo->m_matWorld);
		m_vViewPos = m_vViewTarget + (vTemp);

		m_bChangeInfo = true;
	}

	// 카메라는 플레이어를 기준으로 움직이기 때문에 
	// 트렌스폼 컴포넌트 업데이트는 따로 하지 않고
	// 멤버 변수만 사용한다.
	// Engine::CGameObject::Update();

	//m_vViewPos = m_pInfo->m_vPos;
	SetView(&m_vViewPos, &m_vViewTarget, D3DXVECTOR3(0.f, 1.f, 0.f));
	SetProj(1.f, 1000.f);

	// 상태가 변경 되었을 때만 매트릭스 갱신
	if (m_bChangeInfo)
	{
		Engine::Get_SubjectMgr()->Notify(MESSAGE_VIEW_MATRIX, &m_matView);
		Engine::Get_SubjectMgr()->Notify(MESSAGE_PROJ_MATRIX, &m_matProj);
	}
		
}

void CPlayerView::Render(void)
{
}

CPlayerView * CPlayerView::Create(LPDIRECT3DDEVICE9 pDevice)
{
	CPlayerView*		pObj = new CPlayerView(pDevice);
	if (FAILED(pObj->Initialize()))
	{
		Engine::Safe_Delete(pObj);
	}

	return pObj;
}

HRESULT CPlayerView::AddComponent(void)
{
	Engine::CComponent*		pComponent = NULL;
	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_pInfo->m_vPos = m_vViewPos;
	m_mapComponent.insert(MAPCOMPONENT::value_type(L"Transform", pComponent));

	Engine::Get_SubjectMgr()->AddData(MESSAGE_VIEW_MATRIX, &m_matView);
	Engine::Get_SubjectMgr()->AddData(MESSAGE_PROJ_MATRIX, &m_matProj);

	return S_OK;
}

void CPlayerView::Release(void)
{
}

void CPlayerView::SetView(D3DXVECTOR3* vPos, D3DXVECTOR3* vTarget, D3DXVECTOR3 vUp)
{
	// 구현하기
	//D3DXMatrixLookAtLH(&m_matView, vPos, vTarget, &vUp);

	// 뷰 메트릭스 구하기
	Engine::CPipeline::MakeViewMatrix(&m_matView, vPos, vTarget, &vUp);
	
	//m_pDevice->SetTransform(D3DTS_VIEW, &m_matView);
}

void CPlayerView::SetProj(float fMin, float fMax)
{
	Engine::CPipeline::MakeProjectionMatrix(&m_matProj, D3DXToRadian(m_fFovY), float(WINCX) / WINCY
		, fMin, fMax);

	//m_pDevice->SetTransform(D3DTS_PROJECTION, &m_matProj);
}

void CPlayerView::KeyCheck(void)
{
	float	fTime = Engine::Get_TimeMgr()->GetTime();;
	float fX, fY;

	// 카메라 X 축 회전
	if (GetAsyncKeyState(VK_UP))
	{
		fX = m_pInfo->m_fAnlge[Engine::ANGLE_X] += D3DXToRadian(90.f) * fTime;

		m_bChangeInfo = true;
	}

	if (GetAsyncKeyState(VK_DOWN))
	{
		fX = m_pInfo->m_fAnlge[Engine::ANGLE_X] -= D3DXToRadian(90.f) * fTime;
		m_bChangeInfo = true;
	}

	// 카메라 Y축 회전
	if (GetAsyncKeyState(VK_RIGHT))
	{
		fY = m_pInfo->m_fAnlge[Engine::ANGLE_Y] -= D3DXToRadian(90.f) * fTime;
		m_bChangeInfo = true;
	}
	
	if (GetAsyncKeyState(VK_LEFT))
	{
		fY = m_pInfo->m_fAnlge[Engine::ANGLE_Y] += D3DXToRadian(90.f) * fTime;
		m_bChangeInfo = true;
	}

	D3DXVECTOR3 vTemp = m_vViewPos - m_vViewTarget;
	D3DXVec3Normalize(&vTemp, &vTemp);

	// 줌인
	if (GetAsyncKeyState(VK_ADD))
	{
		m_fFovY -= 5.f * fTime;

		if (0.1f > m_fFovY)
			m_fFovY = 0.1f;

		m_bChangeInfo = true;
	}

	// 줌아웃
	if (GetAsyncKeyState(VK_SUBTRACT))
	{
		m_fFovY += 5.f * fTime;

		if (120.f < m_fFovY)
			m_fFovY = 120.f;

		m_bChangeInfo = true;
	}
}
