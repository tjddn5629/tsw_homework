#include "stdafx.h"
#include "InfoObserver.h"
#include "Include.h"
#include "Export_Function.h"

void CInfoObserver::Update(int _iMessage, void * _pData)
{
	list<void*>* pDataList = Engine::Get_SubjectMgr()->GetDataList(_iMessage);

	if (pDataList == NULL)
		return;

	list<void*>::iterator iter = find(pDataList->begin(), pDataList->end(), _pData);

	switch (_iMessage)
	{
	case MESSAGE_OBJECT_MATRIX:
	{
		D3DXMATRIX matOri = *((D3DXMATRIX*)(*iter));
		m_matObjectMatrix = matOri*m_matViewMatrix*m_matProjMatrix;
		*((D3DXMATRIX*)(*iter)) = m_matObjectMatrix;
		break;
	}
		

	case MESSAGE_VIEW_MATRIX:
		m_matViewMatrix = *((D3DXMATRIX*)(*iter));
		break;

	case MESSAGE_PROJ_MATRIX:
		m_matProjMatrix = *((D3DXMATRIX*)(*iter));
		break;
	}
}

CInfoObserver::CInfoObserver()
{
}


CInfoObserver::~CInfoObserver()
{
}
