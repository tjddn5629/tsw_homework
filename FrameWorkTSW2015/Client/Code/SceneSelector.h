#ifndef SceneSelector_h__
#define SceneSelector_h__

#include "Include.h"
#include "Logo.h"
#include "Stage.h"

class CSceneSelector
{
public:
	explicit CSceneSelector(SCENEID eSceneID)
		: m_eSceneID(eSceneID) {};
	~CSceneSelector(void) {};


public:
	HRESULT operator()(Engine::CScene** ppScene, LPDIRECT3DDEVICE9 pDevice)
	{
		switch (m_eSceneID)
		{
		case SCENE_LOGO:
			*ppScene = CLogo::Create(pDevice);
			break;

		case SCENE_STAGE:
			*ppScene = CStage::Create(pDevice);
			break;
		}

		return S_OK;
	}

private:
	SCENEID		m_eSceneID;
};

#endif // SceneSelector_h__