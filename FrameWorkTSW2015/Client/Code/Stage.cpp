#include "stdafx.h"
#include "Stage.h"

#include "Export_Function.h"
#include "Include.h"
#include "Layer.h"
#include "Player.h"
#include "PlayerView.h"
#include "Land.h"
#include "SceneSelector.h"
#include "InfoObserver.h"

CStage::CStage(LPDIRECT3DDEVICE9 pDevice)
	: Engine::CScene(pDevice)
	, m_pObserver(NULL)
{
}

CStage::~CStage(void)
{
	Release();
}

void CStage::Update(void)
{
	Engine::CScene::Update();
}

void CStage::Render(void)
{
	Engine::CScene::Render();
}

CStage* CStage::Create(LPDIRECT3DDEVICE9 pDevice)
{
	CStage*	pScene = new CStage(pDevice);
	if (FAILED(pScene->Initialize()))
		Safe_Delete(pScene);

	return pScene;
}

HRESULT CStage::Initialize(void)
{
	HRESULT		hr = NULL;

	hr = Engine::Get_ResourceMgr()->AddTexture(m_pDevice, Engine::RESOURCE_DYNAMIC
		, Engine::TEXTURE_NORMAL
		, L"Texture_Player"
		, L"../bin/Texture/Player%d.png", 1);
	FAILED_CHECK(hr);

	hr = Engine::Get_ResourceMgr()->AddBuffer(m_pDevice
		, Engine::RESOURCE_STATIC
		, Engine::BUFFER_LANDTEX
		, _T("Buffer_LandTex")
		, VTXCNTX
		, VTXCNTZ
		, VTXITV);
	FAILED_CHECK(hr);

	hr = Engine::Get_ResourceMgr()->AddTexture(m_pDevice
		, Engine::RESOURCE_DYNAMIC
		, Engine::TEXTURE_NORMAL
		, L"Texture_StageTerrain"
		, L"../bin/Texture/Terrain/Terrain%d.png", 1);
	FAILED_CHECK(hr);

	hr = Add_Environment_Layer();	FAILED_CHECK(hr);
	hr = Add_GameLogic_Layer();		FAILED_CHECK(hr);
	hr = Add_UI_Layer();			FAILED_CHECK(hr);

	m_pObserver = new CInfoObserver;
	Engine::Get_SubjectMgr()->SetWatch(m_pObserver);

	return S_OK;
}

HRESULT CStage::Add_Environment_Layer(void)
{
	Engine::CLayer*		pLayer = Engine::CLayer::Create();
	NULL_CHECK_RETURN(pLayer, E_FAIL);
	Engine::CGameObject*		pGameObject = NULL;

	pGameObject = CLand::Create(m_pDevice);
	pLayer->AddObject(L"Land", pGameObject);

	m_mapLayer.insert(MAPLAYER::value_type(LAYER_ENVIROMENT, pLayer));

	return S_OK;
}

HRESULT CStage::Add_GameLogic_Layer(void)
{
	Engine::CLayer*		pLayer = Engine::CLayer::Create();
	NULL_CHECK_RETURN(pLayer, E_FAIL);
	Engine::CGameObject*		pGameObject = NULL;

	m_pPlayer = CPlayer::Create(m_pDevice);
	pGameObject = m_pPlayer;
	pLayer->AddObject(L"Player", pGameObject);

	m_mapLayer.insert(MAPLAYER::value_type(LAYER_GAMELOGIC, pLayer));

	// 플레이어 카메라
	pGameObject = CPlayerView::Create(m_pDevice);
	pLayer->AddObject(L"PlayerView", pGameObject);
	dynamic_cast<CPlayerView*>(pGameObject)->SetPlayer(m_pPlayer);
	m_mapLayer.insert(MAPLAYER::value_type(LAYER_GAMELOGIC, pLayer));
	return S_OK;
}

HRESULT CStage::Add_UI_Layer(void)
{
	return S_OK;
}

void CStage::Release(void)
{

}