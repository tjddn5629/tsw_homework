#include "stdafx.h"
#include "MainApp.h"

#include "Export_Function.h"
#include "VIBuffer.h"
#include "SceneSelector.h"

CMainApp::CMainApp(void)
: m_pDevice(NULL)
, m_pGraphicDev(Engine::Get_GraphicDev())
, m_pTimeMgr(Engine::Get_TimeMgr())
, m_pSceneMgr(Engine::Get_Management())
, m_pResourceMgr(Engine::Get_ResourceMgr())
, m_pKeyMgr(Engine::Get_KeyMgr())
, m_pInfoSubject(Engine::Get_SubjectMgr())
{
}

CMainApp::~CMainApp(void)
{
	Release();
}

HRESULT CMainApp::InitApp(void)
{
	m_fAngleY = 0.f;
	m_fAngleX = 0.f;
	m_pKeyMgr->SetKeyState();
	// 랜덤용
	srand((unsigned int)time(NULL));

	HRESULT hr = m_pGraphicDev->InitGraphicDev(Engine::CGraphicDev::MODE_WIN
		, g_hWnd, WINCX, WINCY);

	m_pDevice = m_pGraphicDev->GetDevice();	
	
	m_pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	//m_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

	////------------------------------------------------------------
	//m_vViewPos = D3DXVECTOR3(0.f, 50.f, -150.f);
	//m_vViewLook = m_vViewPos;
	//m_vViewTarget = D3DXVECTOR3(0.f, 0.f, 0.f);
	//SetView(m_vViewPos, m_vViewTarget, D3DXVECTOR3(0.f, 1.f, 0.f));
	//SetProj(1.f, 1000.f);
	////------------------------------------------------------------

	Engine::Get_ResourceMgr()->AddBuffer(m_pDevice, Engine::RESOURCE_STATIC, Engine::BUFFER_RCTEX
		, L"Buffer_RcTex");
	
	m_pTimeMgr->InitTime();

	hr = m_pSceneMgr->InitManagement(m_pDevice);
	FAILED_CHECK(hr);
	hr = m_pSceneMgr->SceneChange(CSceneSelector(SCENE_LOGO), m_pDevice);
	FAILED_CHECK(hr);

	return S_OK;
}

void CMainApp::Update(void)
{	
	m_pKeyMgr->SetKeyState();
	//KeyCheck();
	m_pTimeMgr->SetTime();
	m_pSceneMgr->Update();	
}

void CMainApp::Render(void)
{
	m_pDevice->Clear(0, NULL
		, D3DCLEAR_STENCIL | D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 255)
		, 1.f, 0);
	m_pDevice->BeginScene();
	/////////////////////////////////////////////////////////////////////
	
	m_pSceneMgr->Render(m_pTimeMgr->GetTime());
	
	//////////////////////////////////////////////////////////////////////
	m_pDevice->EndScene();
	m_pDevice->Present(NULL, NULL, NULL, NULL);
}

CMainApp* CMainApp::Create(void)
{
	CMainApp*	pMainApp = new CMainApp;
	if(FAILED(pMainApp->InitApp()))
	{
		delete pMainApp;
		pMainApp = NULL;
	}
	return pMainApp;
}

void CMainApp::Release(void)
{
	m_pGraphicDev->DestroyInstance();
	m_pTimeMgr->DestroyInstance();
	m_pResourceMgr->DestroyInstance();
	m_pSceneMgr->DestroyInstance();
	m_pKeyMgr->DestroyInstance();
	m_pInfoSubject->DestroyInstance();
}


void CMainApp::SetView(D3DXVECTOR3 vPos, D3DXVECTOR3 vTarget, D3DXVECTOR3 vUp)
{
	D3DXMatrixLookAtLH(&m_matView, &vPos, &vTarget, &vUp);
	m_pDevice->SetTransform(D3DTS_VIEW, &m_matView);
}

void CMainApp::SetProj(float fMin, float fMax)
{
	D3DXMatrixPerspectiveFovLH(
		&m_matProj,
		D3DXToRadian(45.f),	// 투영 각도
		WINCX / WINCY,
		fMin,
		fMax);
	m_pDevice->SetTransform(D3DTS_PROJECTION, &m_matProj);
}

void CMainApp::KeyCheck(void)
{
	D3DXMATRIX	matRotY, matRotX, matRot;
	D3DXVECTOR3 vDir;

	// 카메라 회전
	if (m_pKeyMgr->GetKeyState(Engine::KEY_D_DOWN))
		m_fAngleY += D3DXToRadian(0.3f);

	if (m_pKeyMgr->GetKeyState(Engine::KEY_A_DOWN))
		m_fAngleY -= D3DXToRadian(0.3f);

	if (m_pKeyMgr->GetKeyState(Engine::KEY_W_DOWN)
		&& m_pKeyMgr->GetKeyState(Engine::KEY_LSHIFT_DOWN))
		m_fAngleX -= D3DXToRadian(0.3f);

	if (m_pKeyMgr->GetKeyState(Engine::KEY_S_DOWN)
		&& m_pKeyMgr->GetKeyState(Engine::KEY_LSHIFT_DOWN))
		m_fAngleX += D3DXToRadian(0.3f);
	
	D3DXMatrixRotationY(&matRotY, m_fAngleY);
	D3DXMatrixRotationX(&matRotX, m_fAngleX);

	matRot = matRotX*matRotY;

	D3DXVECTOR3 vTemp;
	D3DXVec3TransformNormal(&vTemp, &m_vViewLook, &matRot);

	m_vViewTarget = m_vViewPos + (-vTemp * 20.f);

	// 이동 적용
	// 전진
	if (m_pKeyMgr->GetKeyState(Engine::KEY_W_DOWN)
		&& !m_pKeyMgr->GetKeyState(Engine::KEY_LSHIFT_DOWN))
	{
		m_vViewTarget -= vTemp * m_pTimeMgr->GetTime();
		m_vViewPos -= vTemp * m_pTimeMgr->GetTime();
	}

	// 후진
	if (m_pKeyMgr->GetKeyState(Engine::KEY_S_DOWN)
		&& !m_pKeyMgr->GetKeyState(Engine::KEY_LSHIFT_DOWN))
	{
		m_vViewTarget += vTemp * m_pTimeMgr->GetTime();
		m_vViewPos += vTemp * m_pTimeMgr->GetTime();
	}

	// 좌표 이동
	{
		D3DXVECTOR3	vMove = m_vViewTarget - m_vViewPos;
		D3DXVECTOR3 vRight;
		vMove.y = 0.f;	// 높이값 필요 없음
		
		D3DXVec3Cross(&vRight, &vMove, &D3DXVECTOR3(0.f, 1.f, 0.f));

		D3DXVec3Normalize(&vMove, &vMove);
		D3DXVec3Normalize(&vRight, &vRight);

		if (m_pKeyMgr->GetKeyState(Engine::KEY_UP))
		{
			m_vViewPos += vMove * 20.f * m_pTimeMgr->GetTime();
			m_vViewTarget += vMove * 20.f * m_pTimeMgr->GetTime();
		}

		if (m_pKeyMgr->GetKeyState(Engine::KEY_DOWN))
		{
			m_vViewPos -= vMove * 20.f * m_pTimeMgr->GetTime();
			m_vViewTarget -= vMove * 20.f * m_pTimeMgr->GetTime();
		}

		if (m_pKeyMgr->GetKeyState(Engine::KEY_LEFT))
		{
			m_vViewPos += vRight * 20.f * m_pTimeMgr->GetTime();
			m_vViewTarget += vRight * 20.f * m_pTimeMgr->GetTime();
		}

		if (m_pKeyMgr->GetKeyState(Engine::KEY_RIGHT))
		{
			m_vViewPos -= vRight * 20.f * m_pTimeMgr->GetTime();
			m_vViewTarget -= vRight * 20.f * m_pTimeMgr->GetTime();
		}
	}

	SetView(m_vViewPos, m_vViewTarget, D3DXVECTOR3(0.f, 1.f, 0.f));
}

