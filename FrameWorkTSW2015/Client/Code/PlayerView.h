#ifndef PlayerView_h__
#define PlayerView_h__

#include "GameObject.h"

namespace Engine
{
	class CTransform;
}

class CPlayer;
class CPlayerView :
	public Engine::CGameObject
{
private:
	explicit CPlayerView(LPDIRECT3DDEVICE9 pDevice);

public:
	virtual ~CPlayerView(void);

public:
	virtual void Update(void);
	virtual void Render(void);

public:
	static CPlayerView* Create(LPDIRECT3DDEVICE9 pDevice);
	
	void SetPlayer(CPlayer* pObj) { m_pPlayer = pObj; }

private:
	HRESULT Initialize(void);
	HRESULT AddComponent(void);
	void Release(void);

	void SetView(D3DXVECTOR3* vPos, D3DXVECTOR3* vTarget, D3DXVECTOR3 vUp);
	void SetProj(float fMin, float fMax);

	void KeyCheck(void);

private:
	Engine::CTransform*		m_pInfo;

private:
	D3DXMATRIX			m_matView;
	D3DXMATRIX			m_matProj;

	D3DXVECTOR3			m_vViewPos;
	D3DXVECTOR3			m_vViewLook;
	D3DXVECTOR3			m_vViewTarget;

	float				m_fAngleY;
	float				m_fAngleX;

	float				m_fFovY;

private:
	CPlayer*			m_pPlayer;

private:
	bool					m_bChangeInfo;
};


#endif // !PlayerView_h__

