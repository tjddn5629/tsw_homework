#include "stdafx.h"
#include "Land.h"

#include "Include.h"
#include "Export_Function.h"
#include "VIBuffer.h"
#include "Texture.h"
#include "Transform.h"

CLand::CLand(LPDIRECT3DDEVICE9 pDevice)
: Engine::CGameObject(pDevice)
, m_pBuffer(NULL)
, m_pTexture(NULL)
{
}


CLand::~CLand()
{
	Release();
}

HRESULT CLand::Initialize(void)
{
	HRESULT		hr = NULL;

	hr = AddComponent();
	FAILED_CHECK(hr);

	return S_OK;
}

void CLand::Update(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	Engine::CGameObject::Update();
	
}

void CLand::Render(void)
{
	//m_pDevice->SetTransform(D3DTS_WORLD, &m_pInfo->m_matWorld);
	Engine::Get_SubjectMgr()->Notify(MESSAGE_OBJECT_MATRIX, &m_pInfo->m_matWorld);
	m_pTexture->Render(0, 0);
	m_pBuffer->Render();
}

CLand * CLand::Create(LPDIRECT3DDEVICE9 pDevice)
{
	CLand* pObj = new CLand(pDevice);
	if (FAILED(pObj->Initialize()))
		Engine::Safe_Delete(pObj);

	return pObj;
}

HRESULT CLand::AddComponent(void)
{
	Engine::CComponent* pComponent = NULL;
	
	pComponent = Engine::Get_ResourceMgr()->CloneResource(Engine::RESOURCE_DYNAMIC, L"Texture_StageTerrain");
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.insert(MAPCOMPONENT::value_type(L"Texture", pComponent));

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.insert(MAPCOMPONENT::value_type(L"Transform", pComponent));

	pComponent = Engine::Get_ResourceMgr()->CloneResource(Engine::RESOURCE_STATIC, _T("Buffer_LandTex"));
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	m_pBuffer->SetRenderMatrix(&(m_pInfo->m_matWorld));
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.insert(MAPCOMPONENT::value_type(_T("Land"), pComponent));

	Engine::Get_SubjectMgr()->AddData(MESSAGE_OBJECT_MATRIX, &m_pInfo->m_matWorld);

	return S_OK;
}

void CLand::Release(void)
{
}
