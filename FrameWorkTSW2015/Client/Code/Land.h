#ifndef Land_h__
#define Land_h__

#include "GameObject.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTransform;
}

class CLand :
	public Engine::CGameObject
{
private:
	explicit CLand(LPDIRECT3DDEVICE9 pDevice);

public:
	virtual ~CLand(void);

public:
	virtual void Update(void);
	virtual void Render(void);

public:
	static CLand* Create(LPDIRECT3DDEVICE9 pDevice);

private:
	HRESULT Initialize(void);
	HRESULT AddComponent(void);
	void Release(void);

private:
	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;
	Engine::CTransform*		m_pInfo;
};

#endif