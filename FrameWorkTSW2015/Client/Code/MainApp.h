#ifndef MainApp_h__
#define MainApp_h__

#include "Include.h"
#include "Scene.h"

namespace Engine
{
	class CGraphicDev;
	class CTimeMgr;
	class CManagement;
	class CResourceMgr;
	class CKeyMgr;
	class CVIBuffer;
	class CInfoSubject;
}

class CMainApp
{
private:
	CMainApp(void);

public:
	~CMainApp(void);
	
public:
	HRESULT InitApp(void);
	void Update(void);
	void Render(void);

public:
	static CMainApp* Create(void);

private:
	void Release(void);

	void SetView(D3DXVECTOR3 vPos, D3DXVECTOR3 vTarget, D3DXVECTOR3 vUp);
	void SetProj(float fMin, float fMax);

	void KeyCheck(void);

private:
	LPDIRECT3DDEVICE9		m_pDevice;
	
private:
	Engine::CGraphicDev*		m_pGraphicDev;
	Engine::CVIBuffer*			m_pBuffer;
	Engine::CTimeMgr*			m_pTimeMgr;
	Engine::CManagement*		m_pSceneMgr;
	Engine::CResourceMgr*		m_pResourceMgr;
	Engine::CKeyMgr*			m_pKeyMgr;
	Engine::CInfoSubject*		m_pInfoSubject;

private:
	D3DXMATRIX			m_matView;
	D3DXMATRIX			m_matProj;

	D3DXVECTOR3			m_vViewPos;
	D3DXVECTOR3			m_vViewLook;
	D3DXVECTOR3			m_vViewTarget;

	float				m_fAngleY;
	float				m_fAngleX;
};

#endif