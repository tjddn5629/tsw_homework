#include "stdafx.h"
#include "Logo.h"

#include "Export_Function.h"
#include "Include.h"
#include "Layer.h"
#include "GameObject.h"
#include "LogoBack.h"
#include "SceneSelector.h"

CLogo::CLogo(LPDIRECT3DDEVICE9 pDevice)
:Engine::CScene(pDevice)
{
}

CLogo::~CLogo(void)
{
	Release();
}

HRESULT CLogo::Initialize(void)
{
	HRESULT		hr = NULL;

	hr = Engine::Get_ResourceMgr()->AddTexture(m_pDevice
		, Engine::RESOURCE_DYNAMIC
		, Engine::TEXTURE_NORMAL
		, L"Texture_LogoBack"
		, L"../bin/Texture/LogoBack/LogoBack_%d.png", 38);

	FAILED_CHECK(hr);

	hr = Add_Environment_Layer();	FAILED_CHECK(hr);
	hr = Add_GameLogic_Layer();		FAILED_CHECK(hr);
	hr = Add_UI_Layer();			FAILED_CHECK(hr);

	return S_OK;
}

void CLogo::Update(void)
{
	Engine::CScene::Update();

	if (GetAsyncKeyState(VK_RETURN))
	{
		Engine::Get_Management()->SceneChange(CSceneSelector(SCENE_STAGE), m_pDevice);
		return;
	}
}

void CLogo::Render(void)
{
	Engine::CScene::Render();
}

CLogo * CLogo::Create(LPDIRECT3DDEVICE9 pDevice)
{
	CLogo*	pScene = new CLogo(pDevice);
	if (FAILED(pScene->Initialize()))
		Safe_Delete(pScene);

	return pScene;
}

HRESULT CLogo::Add_Environment_Layer(void)
{
	return S_OK;
}

HRESULT CLogo::Add_GameLogic_Layer(void)
{
	Engine::CLayer*		pLayer = Engine::CLayer::Create();
	NULL_CHECK_RETURN(pLayer, E_FAIL);
	Engine::CGameObject*		pGameObject = NULL;

	pGameObject = CLogoBack::Create(m_pDevice);
	pLayer->AddObject(L"LogoBack", pGameObject);

	m_mapLayer.insert(MAPLAYER::value_type(LAYER_GAMELOGIC, pLayer));
	return S_OK;
}

HRESULT CLogo::Add_UI_Layer(void)
{
	return S_OK;
}

void CLogo::Release(void)
{
	Engine::Get_ResourceMgr()->ResetDynamic();
}
